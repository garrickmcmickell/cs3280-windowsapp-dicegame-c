﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Assignment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region "Class Level Variables"
        /// <summary>
        /// A random object to be used for creating the random integers.
        /// </summary>
        Random rand1 = new Random();
        /// <summary>
        /// A random integer that represents the number that the die landed on.
        /// </summary>
        int iRandom;
        /// <summary>
        /// A random integer that is used to simulate a die rolling by changing image file used.
        /// </summary>
        int iRandomDice;
        /// <summary>
        /// An integer to keep track of the amount of times played.
        /// </summary>
        int iTimesPlayedCount = 0;
        /// <summary>
        /// An integer to keep track of the number of times tbEntry has equaled iRandom.
        /// </summary>
        int iTimesWonCount = 0;
        /// <summary>
        /// An integer to keep track of the number of times tbEntry has not equaled iRandom.
        /// </summary>
        int iTimesLostCount = 0;
        /// <summary>
        /// An integer array that keeps track of the frequency of each number rolled.
        /// </summary>
        int[] iArrFreq = new int[6];
        /// <summary>
        /// An integer array that keeps track of number of times each number has been guessed.
        /// </summary>
        int[] iArrGuesses = new int[6];
        /// <summary>
        /// A label array to hold the labels in the Frequency column.
        /// </summary>
        Label[] labArrFreq;
        /// <summary>
        /// A label array to hold the labels in the Percent column.
        /// </summary>
        Label[] labArrPer;
        /// <summary>
        /// A label array to hold the labels in the Times Guessed column. 
        /// </summary>
        Label[] labArrGuess;
        /// <summary>
        /// A boolean to determine if the value in tbEntry is a valid entry. Will prevent btRoll_Click() from doing anything
        /// if it is false. 
        /// </summary>
        bool boolCanParse = false;
        #endregion

        /// <summary>
        /// This method initializes the window and assigns the labels to a label array. It also assigns a default image to image and calls resetLabels()
        /// to set all values to 0.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            labArrFreq = new Label[] { labFreq1, labFreq2, labFreq3, labFreq4, labFreq5, labFreq6 };
            labArrPer = new Label[] { labPer1, labPer2, labPer3, labPer4, labPer5, labPer6 };
            labArrGuess = new Label[] { labGuess1, labGuess2, labGuess3, labGuess4, labGuess5, labGuess6 };
            image.Source = new BitmapImage(new Uri(@"\dice-01.png", UriKind.Relative));

            resetLabels();
        }

        /// <summary>
        /// This method handles the roll button click event. If the value in tbEntry can be used, it will create a random number, compare
        /// that to the value guessed, and calculate/display necessary stats.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btRoll_Click(object sender, RoutedEventArgs e)
        {
            if (boolCanParse)
            {
                iRandom = rand1.Next(1, 7);
                dieRoll(iRandom);

                ///This section updates the values in the Game Info section.
                #region "Game Info"
                iTimesPlayedCount++;
                labTimesPlayedCount.Content = iTimesPlayedCount;

                if (Int32.Parse(tbEntry.Text) == iRandom)
                {
                    iTimesWonCount++;
                    labTimesWonCount.Content = iTimesWonCount;
                }
                else
                {
                    iTimesLostCount++;
                    labTimesLostCount.Content = iTimesLostCount;
                }
                #endregion

                ///This section updates the values in the Frequency column.
                #region "Frequecy Switch"
                switch (iRandom)
                {
                    case 1:
                        iArrFreq[0]++;
                        labArrFreq[0].Content = iArrFreq[0];
                        break;
                    case 2:
                        iArrFreq[1]++;
                        labArrFreq[1].Content = iArrFreq[1];
                        break;
                    case 3:
                        iArrFreq[2]++;
                        labArrFreq[2].Content = iArrFreq[2];
                        break;
                    case 4:
                        iArrFreq[3]++;
                        labArrFreq[3].Content = iArrFreq[3];
                        break;
                    case 5:
                        iArrFreq[4]++;
                        labArrFreq[4].Content = iArrFreq[4];
                        break;
                    case 6:
                        iArrFreq[5]++;
                        labArrFreq[5].Content = iArrFreq[5];
                        break;
                    default:
                        break;
                }
                #endregion

                ///This section updates the values in the Percent column.
                #region "Calculate and Display Percentages"
                labPer1.Content = String.Format("{0:P2}", (double)iArrFreq[0] / iTimesPlayedCount);
                labPer2.Content = String.Format("{0:P2}", (double)iArrFreq[1] / iTimesPlayedCount);
                labPer3.Content = String.Format("{0:P2}", (double)iArrFreq[2] / iTimesPlayedCount);
                labPer4.Content = String.Format("{0:P2}", (double)iArrFreq[3] / iTimesPlayedCount);
                labPer5.Content = String.Format("{0:P2}", (double)iArrFreq[4] / iTimesPlayedCount);
                labPer6.Content = String.Format("{0:P2}", (double)iArrFreq[5] / iTimesPlayedCount);
                #endregion

                ///This section updates the values in the Times Guessed column.
                #region "Times Guessed Switch"
                switch (Int32.Parse(tbEntry.Text))
                {
                    case 1:
                        iArrGuesses[0]++;
                        labArrGuess[0].Content = iArrGuesses[0];
                        break;
                    case 2:
                        iArrGuesses[1]++;
                        labArrGuess[1].Content = iArrGuesses[1];
                        break;
                    case 3:
                        iArrGuesses[2]++;
                        labArrGuess[2].Content = iArrGuesses[2];
                        break;
                    case 4:
                        iArrGuesses[3]++;
                        labArrGuess[3].Content = iArrGuesses[3];
                        break;
                    case 5:
                        iArrGuesses[4]++;
                        labArrGuess[4].Content = iArrGuesses[4];
                        break;
                    case 6:
                        iArrGuesses[5]++;
                        labArrGuess[5].Content = iArrGuesses[5];
                        break;
                    default:
                        break;
                }
                #endregion
            }
        }

        /// <summary>
        /// This method handles values in tbEntry. When changed, it will determine if the value is a valid entry, and if not, will trip a boolean value
        /// that will not allow the entry to be used, and it will display warning text stating that a value is not valid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbEntry.Text.Equals("1") || tbEntry.Text.Equals("2") || tbEntry.Text.Equals("3") || tbEntry.Text.Equals("4") || tbEntry.Text.Equals("5") || tbEntry.Text.Equals("6") || tbEntry.Text.Equals(""))
            {
                labInvalidEntry.Content = "";
                labEnterGuessSub.Foreground = Brushes.White;

                if (tbEntry.Text.Equals(""))
                    boolCanParse = false;
                else
                    boolCanParse = true;
            }
            else
            {
                labInvalidEntry.Content = "That is not a valid entry";
                labEnterGuessSub.Foreground = Brushes.Red;
                boolCanParse = false;
            }

        }

        /// <summary>
        /// This method handles the key "Enter." When pressed, it runs the btRoll_Click() method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btRoll_Click(sender, e);
        }

        /// <summary>
        /// This method handles the reset button click event, which simply calls resetLabels().
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btReset_Click(object sender, RoutedEventArgs e)
        {
            resetLabels();
        }

        /// <summary>
        /// This method resets all of the labels to their original value of 0.
        /// </summary>
        private void resetLabels()
        {
            for (int i = 0; i < 6; i++)
            {
                iArrFreq[i] = 0;
                labArrFreq[i].Content = iArrFreq[i];
                labArrPer[i].Content = 0;
                iArrGuesses[i] = 0;
                labArrGuess[i].Content = iArrGuesses[0];
            }

            iTimesPlayedCount = 0;
            iTimesWonCount = 0;
            iTimesLostCount = 0;
            labTimesPlayedCount.Content = iTimesPlayedCount;
            labTimesWonCount.Content = iTimesWonCount;
            labTimesLostCount.Content = iTimesLostCount;
        }

        /// <summary>
        /// This method updates the image to the iRandom's number. 
        /// 
        /// This is supposed to receive an integer and then cycle through different images to simulate a die being rolled,
        /// but it doesn't work, so I took out the Thread.Sleep() method. Also, it seems to be continuously creating new
        /// objects and wasting space, but I left it as is.
        /// </summary>
        /// <param name="iEndOnThis">iRandom's value, and the die face that the dieRoll() method will end on.</param>
        private void dieRoll(int iEndOnThis)
        {
            for (int i = 0; i < 7; i++)
            {
                iRandomDice = rand1.Next(1, 7);
                image.Source = new BitmapImage(new Uri(@"\dice-0" + iRandomDice + ".png", UriKind.RelativeOrAbsolute));
            }
            image.Source = new BitmapImage(new Uri(@"\dice-0" + iEndOnThis + ".png", UriKind.RelativeOrAbsolute));
        }

    }

}
